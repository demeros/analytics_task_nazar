'use strict';

const Lab = require('lab');
const lab = exports.lab = Lab.script();

const { describe, it, beforeEach, afterEach } = lab;
const { expect } = require('code');
const { stub, assert } = require('sinon');

const repository = require('../../../database/events/pageViewsEventRepository');

const noop = () => {
};
const logger = {
    debug: noop,
    error: noop
};

describe('eventsRepository', () => {

    const suite = {
        logger: logger,
        getConnection: stub(),
        connection: {
            query: stub(),
            release: stub()
        }
    };

    beforeEach((done) => {
        const { create, getAll } = repository({ logger, dbPool: suite });

        suite.create = create;
        suite.getAll = getAll;
        suite.getConnection.callsFake((callback) => {
            callback(undefined, suite.connection);
        });

        suite.connection.query.callsFake((query, params, callback) => {
            callback(undefined, {});
        });

        suite.stubPool = suite.getConnection;
        done();
    });

    afterEach((done) => {
        suite.getConnection.resetHistory();
        suite.connection.query.resetHistory();
        suite.connection.release.resetHistory();

        done();
    });

    describe('#create', () => {
        describe('When creation was successful', () => {
            it('will resolve `event` with original value', () => {
                const event = {
                    'page-referrer': 'localhost',
                    'user-ip': '5.199.237.48',
                    'page-id': 'page',
                    type: 'page-view'
                };

                return suite.create(event)
                    .then((output) => {
                        expect(output).to.be.equal(event);
                        assert.calledOnce(suite.getConnection);
                        assert.calledOnce(suite.connection.release);
                        assert.calledWith(
                            suite.connection.query,
                            'INSERT INTO `event` (`page-referrer`,`user-ip`,`page-id`,`type`) VALUES (?,?,?,?)',
                            ['localhost', '5.199.237.48', 'page', 'page-view']);
                    });
            });
        });
    });

    describe('#getAll', () => {
        describe('When operation was successful', () => {
            it('will resolve array of event', () => {
                return suite.getAll({ pagination: { limit: 2, skip: 2 } })
                    .then((result) => {
                        expect(result).to.be.equal({});
                        assert.calledOnce(suite.getConnection);
                        assert.calledOnce(suite.connection.release);
                        assert.calledWith(
                            suite.connection.query,
                            'SELECT * FROM `event`  LIMIT 2 OFFSET 2'
                        );
                    });
            });
        });
    });
});
