'use strict';

const Lab = require('lab');
const lab = exports.lab = Lab.script();

const { describe, it } = lab;
const { expect } = require('code');

const requestHelper = require('../../utils/requestHelper');

describe('requestHelper', () => {
    describe('#getPagination', () => {
        describe('when `skip` and `limit` not provided', () => {
            it('will return pagination object with default values', (done) => {
                const result = requestHelper.getPagination({});
                expect(result).to.be.equal({ limit: 25, skip: 0 });
                done();
            });
        });

        describe('when `skip` is negative value', () => {
            it('will return pagination object with default values', (done) => {
                const result = requestHelper.getPagination({ skip: -5 });
                expect(result).to.be.equal({ limit: 25, skip: 0 });
                done();
            });
        });

        describe('when `limit` is negative value', () => {
            it('will return pagination object with default values', (done) => {
                const result = requestHelper.getPagination({ limit: -5 });
                expect(result).to.be.equal({ limit: 25, skip: 0 });
                done();
            });
        });

        describe('when `limit` has correct value', () => {
            it('will return pagination object with custom `limit` value', (done) => {
                const result = requestHelper.getPagination({ limit: 5 });
                expect(result).to.be.equal({ limit: 5, skip: 0 });
                done();
            });
        });

        describe('when `skip` has correct value', () => {
            it('will return pagination object with custom `skip` value', (done) => {
                const result = requestHelper.getPagination({ skip: 5 });
                expect(result).to.be.equal({ limit: 25, skip: 5 });
                done();
            });
        });

        describe('when `skip` and `limit` has correct values', () => {
            it('will return pagination object with custom `skip` and `limit` values', (done) => {
                const result = requestHelper.getPagination({ limit: 3, skip: 5 });
                expect(result).to.be.equal({ limit: 3, skip: 5 });
                done();
            });
        });
    });
});
