'use strict';

const Lab = require('lab');
const lab = exports.lab = Lab.script();

const { describe, it, fail } = lab;
const { expect } = require('code');

const eventValidator = require('../../../utils/validators/eventValidator');

describe('eventValidator', () => {

    describe('#validate', () => {
        describe('when `event` is empty object', () => {
            it('will be rejected with error', () => {
                return eventValidator
                    .validate({})
                    .then(fail)
                    .catch((err) => expect(err).to.be.equal('Object cannot be empty'));
            });
        });

        describe('when `event` was not provided', () => {
            it('will be rejected with error', () => {
                return eventValidator
                    .validate()
                    .then(fail)
                    .catch((err) => expect(err).to.be.equal('Object cannot be empty'));
            });
        });

        describe('when valid `event` was provided', () => {
            it('will be resolved with `event` object', () => {
                const input = {
                    'page-referrer': 'localhost',
                    'user-ip': '5.199.237.48',
                    'page-id': 'page',
                    type: 'page-view'
                };

                return eventValidator
                    .validate(input)
                    .then((output) => {
                        expect(output).to.be.equal(input);
                    })
                    .catch(fail);
            });
        });

        describe('when was provided `event` with redundant fields', () => {
            it('will be resolved with allowed `event` object fields', () => {
                const input = {
                    'page-referrer': 'localhost',
                    'user-ip': '5.199.237.48',
                    'page-id': 'page',
                    type: 'page-view',
                    someField: 'some value'
                };

                return eventValidator
                    .validate(input)
                    .then((output) => {
                        expect(output).to.be.equal({
                            'page-referrer': 'localhost',
                            'user-ip': '5.199.237.48',
                            'page-id': 'page',
                            type: 'page-view',
                        });
                    })
                    .catch(fail);
            });
        });

        describe('when was provided `event` with missed required fields', () => {
            it('will be rejected with validation error', () => {
                const input = {
                    'page-referrer': 'localhost',
                    'user-ip': '5.199.237.48'
                };

                return eventValidator
                    .validate(input)
                    .then(fail)
                    .catch((validationMessage) => {
                        // eslint-disable-next-line
                        expect(validationMessage).to.be.equal('\'page-id\' field is required,\n\'type\' field value is invalid, only \'page-view\' supported');
                    });
            });
        });

        describe('when was provided `event` with invalid field(s) values', () => {
            it('will be rejected with validation error', () => {
                const input = {
                    'page-referrer': 'some host',
                    'user-ip': 'some ip',
                    'page-id': 'page',
                    type: 'page-view',
                };

                return eventValidator
                    .validate(input)
                    .then(fail)
                    .catch((validationMessage) => {
                        // eslint-disable-next-line
                        expect(validationMessage).to.be.equal('\'user-ip\' field value is invalid');
                    });
            });
        });
    });
});
