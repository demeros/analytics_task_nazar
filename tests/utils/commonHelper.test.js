'use strict';

const Lab = require('lab');
const lab = exports.lab = Lab.script();

const { describe, it } = lab;
const { expect } = require('code');

const commonHelper = require('../../utils/commonHelper');

describe('commonHelper', () => {
    describe('#pick', () => {
        describe('when `value` parameter is not an object', () => {
            it('will return empty object', (done) => {
                const result = commonHelper.pick('string', {});
                expect(result).to.be.equal({});
                done();
            });
        });

        describe('when `keys` parameter is not an array', () => {
            it('will return empty object', (done) => {
                const result = commonHelper.pick({}, {});
                expect(result).to.be.equal({});
                done();
            });
        });

        describe('when both arguments are correct', () => {
            describe('and only one property from `keys` present in `value`', () => {
                it('will return new object with one property', (done) => {
                    const result = commonHelper.pick({ a: 1, b: 2 }, ['a', 'c']);
                    expect(result).to.be.equal({ a: 1 });
                    done();
                });
            });

            describe('and all properties from `keys` present in `value`', () => {
                it('will return new object with all properties from `keys`', (done) => {
                    const result = commonHelper.pick({ a: 1, b: 2 }, ['a', 'b']);
                    expect(result).to.be.equal({ a: 1, b: 2 });
                    done();
                });
            });

            describe('and no properties from `keys` present in `value`', () => {
                it('will return new object with all properties from `keys`', (done) => {
                    const result = commonHelper.pick({ a: 1, b: 2 }, ['c', 'd']);
                    expect(result).to.be.equal({});
                    done();
                });
            });
        });
    });

    describe('#isValidIp', () => {
        describe('when ip is valid IPv4', () => {
            it('will return true', (done) => {
                const result = commonHelper.isValidIp('127.0.0.1');
                expect(result).to.be.true();
                done();
            });
        });

        describe('when ip is valid IPv6', () => {
            it('will return true', (done) => {
                const result = commonHelper.isValidIp('2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d');
                expect(result).to.be.true();
                done();
            });
        });

        describe('when ip is invalid', () => {
            it('will return false', (done) => {
                const result = commonHelper.isValidIp('invalid_ip');
                expect(result).to.be.false();
                done();
            });
        });
    });
});
