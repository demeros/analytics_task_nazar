'use strict';

const Lab = require('lab');
const lab = exports.lab = Lab.script();

const { describe, it, before } = lab;
const { expect } = require('code');

const noop = () => {
};

const logger = {
    info: noop,
};

const { tokenAuthorizationFilter } = require('../../../utils/filters')({ logger });

describe('tokenAuthorization', () => {

    const suite = {};

    describe('with valid Authorization header', () => {
        before((done) => {
            suite.req = {
                headers: {
                    Authorization: '6i2nSgWu0DfYIE8I0ZBJOtxTmHJATRzu'
                }
            };

            done();
        });

        it('will call success callback', (done) => {
            tokenAuthorizationFilter(suite.req, {}, done);
        });
    });

    describe('with invalid Authorization header', () => {
        before((done) => {
            suite.req = {
                headers: {
                    Authorization: 'wrong token value'
                }
            };

            done();
        });

        it('will call callback with access forbidden error', (done) => {
            const callback = (error) => {
                expect(error).to.be.equal({ status: 403, message: 'Forbidden' });
                done();
            };

            tokenAuthorizationFilter(suite.req, {}, callback);
        });
    });

    describe('with empty Authorization header', () => {
        before((done) => {
            suite.req = {
                headers: {}
            };

            done();
        });

        it('will call callback with access forbidden error', (done) => {
            const callback = (error) => {
                expect(error).to.be.equal({ status: 403, message: 'Forbidden' });
                done();
            };

            tokenAuthorizationFilter(suite.req, {}, callback);
        });
    });
});
