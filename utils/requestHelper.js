'use strict';

const PARAM_LIMIT = 'limit';
const PARAM_SKIP = 'skip';

const DEFAULT_LIMIT = 25;
const DEFAULT_SKIP = 0;

/**
 * Extract query parameters from request, and prepare them for pagination
 *
 * @param queryParams
 * @return {{skip: number, limit: number}}
 */
const getPagination = (queryParams) => {
    let limit = DEFAULT_LIMIT;
    let skip = DEFAULT_SKIP;

    if (queryParams) {
        const limitParam = parseInt(queryParams[PARAM_LIMIT]);
        limit = limitParam > 0 ? limitParam : limit;

        const skipParam = parseInt(queryParams[PARAM_SKIP]);
        skip = skipParam > 0 ? skipParam : skip;
    }

    return {
        skip,
        limit
    };
};

module.exports = {
    getPagination
};
