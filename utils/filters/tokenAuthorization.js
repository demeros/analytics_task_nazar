'use strict';

const { isSecurityTokenValid } = require('../securityHelpers');
const { FORBIDDEN } = require('../httpCodes');

const AUTH_HEADER_NAME = 'authorization';

module.exports = ({ logger }) => {
    const tokenAuthorizationFilter = (req, res, next) => {
        const token = req.headers[AUTH_HEADER_NAME];

        if (isSecurityTokenValid(token)) {
            return next();
        }

        logger.info(`Authorization token: '${token}' is invalid`);

        next({ status: FORBIDDEN.code, message: FORBIDDEN.message });
    };

    return tokenAuthorizationFilter;
};
