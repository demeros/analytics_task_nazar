'use strict';

const { pick, isValidUrl, isValidIp } = require('../commonHelper');

const SUPPORTED_EVENT_TYPE = 'page-view';

const PAGE_REFERRER = 'page-referrer';
const PAGE_URL = 'page-url';
const USER_IP = 'user-ip';
const TYPE = 'type';
const PAGE_ID = 'page-id';
const SCREEN_RESOLUTION = 'screen-resolution';

const MODEL_DEFINITION = [
    'timestamp', PAGE_REFERRER, 'user-id', PAGE_URL, PAGE_ID, 'user-agent', SCREEN_RESOLUTION, USER_IP, TYPE
];

const validate = (event) => {
    const executor = (resolve, reject) => {
        if (!event) {
            return reject('Object cannot be empty');
        }

        const validatedEvent = pick(event, MODEL_DEFINITION);
        if (!Object.keys(validatedEvent).length) {
            return reject('Object cannot be empty');
        }

        const errors = [];

        if (!validatedEvent[PAGE_ID]) {
            errors.push(`'${PAGE_ID}' field is required`);
        }

        if (!validatedEvent[TYPE] || validatedEvent[TYPE] !== SUPPORTED_EVENT_TYPE) {
            errors.push(`'${TYPE}' field value is invalid, only '${SUPPORTED_EVENT_TYPE}' supported`);
        }

        if (validatedEvent[PAGE_REFERRER] && !isValidUrl(validatedEvent[PAGE_REFERRER])) {
            errors.push(`'${PAGE_REFERRER}' field value is invalid`);
        }

        if (validatedEvent[PAGE_URL] && !isValidUrl(validatedEvent[PAGE_URL])) {
            errors.push(`'${PAGE_URL}' field value is invalid`);
        }

        if (validatedEvent[USER_IP] && !isValidIp(validatedEvent[USER_IP])) {
            errors.push(`'${USER_IP}' field value is invalid`);
        }

        if (validatedEvent[SCREEN_RESOLUTION]) {
            const width = parseInt(validatedEvent[SCREEN_RESOLUTION].width);
            const height = parseInt(validatedEvent[SCREEN_RESOLUTION].height);

            if (isNaN(width) || isNaN(height)) {
                errors.push(`'${SCREEN_RESOLUTION}' field value is invalid`);
            }
        }

        //@TODO other validation rules, if required

        if (errors.length) {
            return reject(errors.join(',\n'));
        }

        return resolve(validatedEvent);
    };

    return new Promise(executor);
};

module.exports = {
    MODEL_DEFINITION,
    validate
};
