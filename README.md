# Installation

1. Run `git clone git@bitbucket.org:demeros/analytics_task_nazar.git`
2. Run `cd analytics_task_nazar`
3. Run `npm install`
4. Navigate to `config/default.json` and change `db` connection settings (url, port, user, password)
5. To setup database run `npm run setup:db`, this will create database and event table
6. `npm t` to run tests

# Description

### Available API ednpoints

| Methods               | URL        | Description    |
|-----------------------|------------|----------------|
| **GET**, **POST**     | /events/page-views    | Create new events, get list of all events |
| **GET**     | /analytics/page-views/browsers/{browserName}/events              | endpoint to get page-views by browser |
| **GET**     | /analytics/page-views/page-ids/{pageId}/events                   | endpoint to get page-views by page-id |
| **GET**     | /analytics/page-views/countries/{countryName}/events             | endpoint to get page-views by country |

### Entity description
All endpoints consume or return same json object - Event

```
{
    "timestamp": 1502475823360 // Optional, Number, Date in UNIX format
    "page-referrer": "" // Optional, String
    "user-id": "" // Optional, String
    "page-url": "" // Required, String
    "user-agent": "" // Optional, String
    "screen-resolution" : { // Optional, Object
        "width": 1920,
        "heigth": 1600
    }
    "user-ip": "127.0.0.1" // Optional, String - IPv4 and IPv6 formats supported
    "type": "page-view" // Required, String - Type of event, only `page-view` supported
}
```

Browser will be taken from `user-agent`, if provided.

Country - from `user-ip`

### Additional query parameters

| Name                  | Description        | 
|-----------------------|------------|
| skip                  | how many items should be skipped                     | 
| limit                 | How many items should contain response set           | 

Example: `GET /events?limit=10&skip=2`

# TL;TR
## API design

### Expose rest endpoint to collect page-view events.

I started from the point that `Analytics event is a key-value object that contains data on a specific event like page-view.`. 
In case, if all event types has same structure it makes sense to keep them all in one resource - `/events`. But, in scope of current task,
i think, `/events/page-views` will be optimal solution, and resolve future problems in case if there will be more event-types with different structure.

### Expose rest endpoint to get page-views by country|page-id|browser
Since there is resource `/events/page-views` which contains all required data already.
First idea was to provide RPC like interface: `/events/page-views?browser={browserName}&country={countryName}&page-id={pageId}"`.
Which, probably, gives more flexibility in terms of querying, but adds complexity to application logic.

I began to rely on assertion that app `will collect and analyze analytics events`.
So i've assumed that all processed events may be interpreted as a part of `/analytics` resource. 

Since we analyzing events based on `event-type` we will have something like `/analytics/{subjects}` - `/analytics/page-views` in my case.

Next task is to describe `filtering` which `get page-views by country|page-id|browser` is, in REST terms. 
I've started to abstract from idea that each `page-view` event which is under `/analytics` resource may be processed somehow. 
So if we have `/analytics/{subjectss}`, `{subjects}` may contain `{topics}` which represent processing result with some criteria - `/analytics/page-views`,
and my criterias are `browsers`, `counties`, `page-ids`; `/analytics/page-views/browsers` etc.

It's easy now to specificate which topic is interested for us: `/analytics/page-views/browsers/{browsername}`, `/analytics/page-views/page-ids/{pageId}`, `/analytics/page-views/countries/{countryName}`.
But, logically, we should return list of browser, counties and page-ids!
Let's add `/events` to each resource, and BINGO!, we will get what we need - filtered `page-view` events.

Final result:

1. `/analytics/page-views/browsers/{browserName}/events`
1. `/analytics/page-views/page-ids/{pageId}/events`        
1. `/analytics/page-views/countries/{countryName}/events` 
