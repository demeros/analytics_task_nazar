'use strict';

const express = require('express');
const router = express.Router();

const { BAD_REQUEST, OK } = require('../../../../../../../utils/httpCodes');
const { getPagination } = require('../../../../../../../utils/requestHelper');

module.exports = ({ logger, dbPool }) => {

    const pageViewsRepository = require('../../../../../../../database/analytics/pageViewsRepository')({
        logger,
        dbPool
    });
    const { tokenAuthorizationFilter } = require('../../../../../../../utils/filters')({ logger });

    router.get('/page-views/page-ids/:pageId/events', tokenAuthorizationFilter, (req, res, next) => {
        const pagination = getPagination(req.query);

        pageViewsRepository.getByPageId(req.params.pageId, { pagination })
            .then()
            .then((result) => {
                //@TODO map result?
                res.status(OK.code);
                return res.json(result);
            })
            .catch((err) => {
                next({ status: BAD_REQUEST.code, message: err });
            });
    });

    return router;
};
