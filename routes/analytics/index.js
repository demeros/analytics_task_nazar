'use strict';

const ROOT = '/analytics';

const register = ({ expressApp, logger, dbPool }) => {
    const eventsByBrowser = require('./{eventType}/page-views/{type}/browsers/{browserName}/events')({
        logger,
        dbPool
    });

    const eventsByCountry = require('./{eventType}/page-views/{type}/countries/{countryName}/events')({
        logger,
        dbPool
    });

    const eventsByPageId = require('./{eventType}/page-views/{type}/pageIds/{pageId}/events')({ logger, dbPool });

    expressApp.use(ROOT, eventsByBrowser, eventsByCountry, eventsByPageId);
};

module.exports = {
    register
};