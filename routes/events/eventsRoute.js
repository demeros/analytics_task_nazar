'use strict';

const express = require('express');
const router = express.Router();
const { CREATED, OK, BAD_REQUEST } = require('../../utils/httpCodes');
const eventValidator = require('../../utils/validators/eventValidator');
const { getBrowserFromUA, getCountryCodeFromIp, getCountryNameFromCode } = require('../../utils/commonHelper');
const { getPagination } = require('../../utils/requestHelper');

const parseBrowserAndCountry = (event) => {
    const executor = (resolve) => {
        const browser = getBrowserFromUA(event['user-agent']);
        const country = getCountryNameFromCode(getCountryCodeFromIp(event['user-ip']));

        return resolve(Object.assign(event, { browser, country }));
    };
    return new Promise(executor);
};

module.exports = ({ logger, dbPool }) => {
    const eventsRepository = require('../../database/events/pageViewsEventRepository')({ logger, dbPool });
    const { tokenAuthorizationFilter } = require('../../utils/filters')({ logger });

    //CREATE
    router.post('/', (req, res, next) => {
        eventValidator
            .validate(req.body)
            .then(parseBrowserAndCountry)
            .then(eventsRepository.create)
            .then((result) => {
                res.status(CREATED.code);
                res.json(result);
            })
            .catch((err) => {
                next({ status: BAD_REQUEST.code, message: err });
            });

    });

    //GET-ALL
    router.get('/', tokenAuthorizationFilter, (req, res, next) => {
        const pagination = getPagination(req.query);
        eventsRepository
            .getAll({ pagination })
            .then((result) => {
                //@TODO map result?
                res.status(OK.code);
                res.json(result);
            })
            .catch((err) => {
                next({ status: BAD_REQUEST.code, message: err });
            });
    });

    return router;
};
