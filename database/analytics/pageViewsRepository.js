'use strict';

const { getDbConnection, executeSelectQuery, preparePaginationString } = require('../../utils/dbHelper');

const PAGE_VIEW_TYPE = 'page-view';

const pageViewsRepository = ({ logger, dbPool }) => {
    const getByPageId = (pageId, { pagination }) => {
        return getDbConnection(dbPool)
            .then((connection) => {
                logger.debug('Connection is opened');

                const query = 'SELECT * FROM `event` WHERE `type`=' + `'${PAGE_VIEW_TYPE}'` +
                    ' AND `page-id`=?' +
                    preparePaginationString(pagination);

                return executeSelectQuery(connection, query, [pageId])
                    .then((events) => {
                        logger.debug('Query successfully executed');
                        return events;
                    });
            })
            .catch((err) => {
                logger.error(err);
                return Promise.reject(err);
            });
    };

    const getByBrowserName = (browserName, { pagination }) => {
        return getDbConnection(dbPool)
            .then((connection) => {
                logger.debug('Connection is opened');

                const query = 'SELECT * FROM `event` WHERE `type`=' + `'${PAGE_VIEW_TYPE}'` +
                    ' AND `browser`=?' +
                    preparePaginationString(pagination);

                return executeSelectQuery(connection, query, [browserName])
                    .then((events) => {
                        logger.debug('Query successfully executed');
                        return events;
                    });
            })
            .catch((err) => {
                logger.error(err);
                return Promise.reject(err);
            });
    };

    const getByCountryName = (countryName, { pagination }) => {
        return getDbConnection(dbPool)
            .then((connection) => {
                logger.debug('Connection is opened');

                const query = 'SELECT * FROM `event` WHERE `type`=' + `'${PAGE_VIEW_TYPE}'` +
                    ' AND `country`=?' +
                    preparePaginationString(pagination);

                return executeSelectQuery(connection, query, [countryName])
                    .then((events) => {
                        logger.debug('Query successfully executed');
                        return events;
                    });
            })
            .catch((err) => {
                logger.error(err);
                return Promise.reject(err);
            });
    };

    return {
        getByPageId,
        getByBrowserName,
        getByCountryName
    };
};

module.exports = pageViewsRepository;
