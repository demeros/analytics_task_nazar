'use strict';

const {
    prepareObjectForInsert,
    getDbConnection,
    executeInsertQuery,
    executeSelectQuery,
    preparePaginationString
} = require('../../utils/dbHelper');

const eventsRepository = ({ logger, dbPool }) => {
    const create = (event) => {
        return getDbConnection(dbPool)
            .then((connection) => {
                logger.debug('Connection is opened');
                const preparedValue = prepareObjectForInsert(event);
                const fields = preparedValue.fields.join(',');

                const query = `INSERT INTO \`event\` (${fields}) VALUES (${preparedValue.preparedStmt})`;

                return executeInsertQuery(connection, query, preparedValue.values)
                    .then(() => {
                        logger.debug('Query successfully executed');
                        return event;
                    });
            })
            .catch((err) => {
                logger.error(err);
                return Promise.reject(err);
            });
    };

    const getAll = ({ pagination }) => {
        return getDbConnection(dbPool)
            .then((connection) => {
                logger.debug('Connection is opened');

                const query = `SELECT * FROM \`event\` ${preparePaginationString(pagination)}`;

                return executeSelectQuery(connection, query)
                    .then((events) => {
                        logger.debug('Query successfully executed');
                        return events;
                    });
            })
            .catch((err) => {
                logger.error(err);
                return Promise.reject(err);
            });
    };

    return {
        create,
        getAll
    };
};

module.exports = eventsRepository;
