'use strict';

const config = require('../config/default');
config.db.database = null;

const dbPool = require('../database/connection')({ config });
const dbHelper = require('../utils/dbHelper');

console.warn('SETUP DB');

const createDb = 'CREATE DATABASE IF NOT EXISTS `analytics_task_nazar`;';
const createTable = 'CREATE TABLE IF NOT EXISTS `analytics_task_nazar`.`event` (\n' +
    '  `event-id` bigint(20) NOT NULL AUTO_INCREMENT,\n' +
    '  `timestamp` int(11) NOT NULL,\n' +
    '  `page-referrer` varchar(2000) DEFAULT NULL,\n' +
    '  `user-id` varchar(100) DEFAULT NULL,\n' +
    '  `page-url` varchar(2000) DEFAULT NULL,\n' +
    '  `page-id` varchar(100) DEFAULT NULL,\n' +
    '  `user-agent` varchar(500) DEFAULT NULL,\n' +
    '  `screen-resolution` varchar(100) DEFAULT NULL,\n' +
    '  `user-ip` varchar(45) DEFAULT NULL,\n' +
    '  `browser` varchar(40) DEFAULT NULL,\n' +
    '  `country` varchar(40) DEFAULT NULL,\n' +
    '  `type` varchar(50) NOT NULL,\n' +
    '  PRIMARY KEY (`event-id`),\n' +
    '  KEY `idx_browser` (`browser`),\n' +
    '  KEY `idx_page-id` (`page-id`),\n' +
    '  KEY `idx_country` (`country`),\n' +
    '  KEY `idx_type` (`type`)\n' +
    ') ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;';

dbHelper
    .getDbConnection(dbPool)
    .then((connection) => {
        console.warn('Connected to MySQL');

        return dbHelper
            .executeSelectQuery(connection, createDb)
            .then((result) => {
                console.warn('Database Created');
                console.warn(result);
            });
    })
    .then(() => {
        dbHelper
            .getDbConnection(dbPool)
            .then((connection) => {
                console.warn('Connected to MySQL');

                return dbHelper
                    .executeSelectQuery(connection, createTable)
                    .then((result) => {
                        console.warn('Database Created');
                        console.warn(result);
                        process.exit(1);
                    });
            });
    })
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
